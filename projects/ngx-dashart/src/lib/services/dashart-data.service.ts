import {Injectable} from '@angular/core';
import {DashartData} from '../model/vo/dashart-data';
import {BaseCrudService} from 'ngrx-entity-crud';

@Injectable({
  providedIn: 'root'
})
export class DashartDataService extends BaseCrudService<DashartData> {
  protected service = 'http://localhost:3000/' + 'dashart-data';
}
