import {Injectable} from '@angular/core';
import {Dashart} from '../model/vo/dashart';
import {BaseCrudService} from 'ngrx-entity-crud';

@Injectable({
  providedIn: 'root'
})
export class DashartService extends BaseCrudService<Dashart> {
  protected service = 'http://localhost:3000/' + 'dashart';
}
