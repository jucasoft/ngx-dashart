// @dynamic
export class DashartData {
  public id: string = undefined;
  public label: string = undefined;
  public value: number = undefined;
  public group: string = undefined;
  public dashart: string = undefined;

  /**
   * metodo statico utilizzato per recuperare l'id dell'entita.
   */
  static selectId: (item: DashartData) => string = item => item.id;
}
