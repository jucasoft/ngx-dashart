import {GridsterItem, GridsterItemComponentInterface} from 'angular-gridster2';

// @dynamic
export class Dashart implements GridsterItem {
  public id: string = undefined;
  // raggruppamento dei grafici.
  public dashartGroup: string = undefined;

  public type: string = undefined;
  public options: any = undefined;
  public plugins: any[] = undefined;
  public width: string = undefined;
  public height: string = undefined;
  public responsive: boolean = undefined;
  public initialized: boolean = undefined;
  public data: any = undefined;

  public connectionType: string = undefined;
  public connectionName: string = undefined;
  public criteria: string = undefined;

  // implementation GridsterItem
  public cols: number = undefined;
  public rows: number = undefined;
  public x: number = undefined;
  public y: number = undefined;
  public compactEnabled: boolean = undefined;
  public dragEnabled: boolean = undefined;
  public initCallback: (item: GridsterItem, itemComponent: GridsterItemComponentInterface) => void = undefined;
  public maxItemArea: number = undefined;
  public maxItemCols: number = undefined;
  public maxItemRows: number = undefined;
  public minItemArea: number = undefined;
  public minItemCols: number = undefined;
  public minItemRows: number = undefined;
  public resizeEnabled: boolean = undefined;

  /**
   * metodo statico utilizzato per recuperare l'id dell'entita.
   */
  static selectId: (item: Dashart) => string = item => item.id;
}
