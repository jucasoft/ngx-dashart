import {ModuleWithProviders, NgModule} from '@angular/core';
import {DashartComponent} from './components/dashart/dashart.component';
import {CommonModule} from '@angular/common';
import {ChartModule} from 'primeng/chart';
import {DashartItemComponent} from './components/dashart-item/dashart-item.component';
import {NgLetModule} from './directive/ng-let.directive';
import {CardModule} from 'primeng/card';
import {ButtonModule} from 'primeng/button';
import {SplitButtonModule} from 'primeng/splitbutton';
import {ContextMenuModule} from 'primeng/contextmenu';
import {GridsterModule} from 'angular-gridster2';
import {PanelModule} from 'primeng/panel';

@NgModule({
  declarations: [DashartComponent, DashartItemComponent],
  imports: [
    CommonModule,
    NgLetModule,
    ChartModule,
    CardModule,
    ButtonModule,
    SplitButtonModule,
    ContextMenuModule,
    GridsterModule,
    PanelModule
  ],
  exports: [DashartComponent]
})
export class NgxDashartModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: NgxDashartModule
    };
  }
}
