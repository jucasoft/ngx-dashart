import {createCrudEntityAdapter, EntityCrudAdapter, EntityCrudState} from 'ngrx-entity-crud';
import {DashartData} from '../../model/vo/dashart-data';

export const adapter: EntityCrudAdapter<DashartData> = createCrudEntityAdapter<DashartData>({
  selectId: model => DashartData.selectId(model),
});

export interface State extends EntityCrudState<DashartData> {
};

export const initialState: State = adapter.getInitialCrudState();
