import {InjectionToken, ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ActionReducer, StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';
import {DashartDataStoreEffects} from './effects';
import {featureReducer} from './reducer';
import {State} from './state';
import {Names} from './names';

export const INJECTION_TOKEN = new InjectionToken<ActionReducer<State>>(`${Names.NAME}-store Reducers`);
// @dynamic
@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature(Names.NAME, INJECTION_TOKEN),
    EffectsModule.forFeature([DashartDataStoreEffects]),
  ],
  declarations: [],
  providers: [
    DashartDataStoreEffects,
    {
      provide: INJECTION_TOKEN,
      useFactory: (): ActionReducer<State> => featureReducer
    }
  ]
})
export class DashartDataStoreModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: DashartDataStoreModule,
      providers: [
        DashartDataStoreEffects,
        {
          provide: INJECTION_TOKEN,
          useFactory: (): ActionReducer<State> => featureReducer
        }
      ]
    };
  }
}
