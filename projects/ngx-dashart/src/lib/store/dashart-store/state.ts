import {createCrudEntityAdapter, EntityCrudAdapter, EntityCrudState} from 'ngrx-entity-crud';
import {Dashart} from '../../model/vo/dashart';

export const adapter: EntityCrudAdapter<Dashart> = createCrudEntityAdapter<Dashart>({
  selectId: model => Dashart.selectId(model),
});

export interface State extends EntityCrudState<Dashart> {
};

export const initialState: State = adapter.getInitialCrudState();
