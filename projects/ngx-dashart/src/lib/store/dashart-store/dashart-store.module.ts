import {InjectionToken, ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ActionReducer, StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';
import {DashartStoreEffects} from './effects';
import {featureReducer} from './reducer';
import {State} from './state';
import {Names} from './names';
import {DashartDataStoreEffects} from '../dashart-data-store/effects';

export const INJECTION_TOKEN = new InjectionToken<ActionReducer<State>>(`${Names.NAME}-store Reducers`);

// @dynamic
@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature(Names.NAME, INJECTION_TOKEN),
    EffectsModule.forFeature([DashartStoreEffects]),
  ],
  declarations: [],
  providers: [DashartStoreEffects,
    {
      provide: INJECTION_TOKEN,
      useFactory: (): ActionReducer<State> => featureReducer
    }]
})
export class DashartStoreModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: DashartStoreModule,
      providers: [
        DashartDataStoreEffects,
        {
          provide: INJECTION_TOKEN,
          useFactory: (): ActionReducer<State> => featureReducer
        }
      ]
    };
  }
}
