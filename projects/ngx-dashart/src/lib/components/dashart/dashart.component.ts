import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {Store} from '@ngrx/store';
import {Dashart} from '../../model/vo/dashart';
import {ConfirmationService} from 'primeng/api';
import {GridsterComponent, GridsterConfig} from 'angular-gridster2';

@Component({
  selector: 'app-dashart',
  templateUrl: './dashart.component.html',
  styles: [`
    :host {
      border: 2px solid dimgray;
      display: block;
      height: 100%;
      padding: 20px;
    }
  `],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None
})
export class DashartComponent implements OnInit {

  menuItems: any;
  @Input() collection: Dashart[];
  @Input() gridsterConfig: GridsterConfig;

  @Output() edit = new EventEmitter<Dashart>();
  @Output() delete = new EventEmitter<Dashart>();
  @Output() init = new EventEmitter<GridsterComponent>();
  @Output() destroy = new EventEmitter<GridsterComponent>();
  @Output() gridSizeChanged = new EventEmitter<GridsterComponent>();

  @Output() itemChange = new EventEmitter<{ item: Dashart, component: GridsterComponent }>();
  @Output() itemResize = new EventEmitter<{ item: Dashart, component: GridsterComponent }>();
  @Output() itemInit = new EventEmitter<{ item: Dashart, component: GridsterComponent }>();
  @Output() itemRemoved = new EventEmitter<{ item: Dashart, component: GridsterComponent }>();
  @Output() itemValidate = new EventEmitter<Dashart>();

  constructor(private store$: Store<any>,
              private confirmationService: ConfirmationService) {
  }

  initCallback(component) {
    if (this.init) {
      this.init.emit(component);
    }
  }

  destroyCallback(component) {
    if (this.destroy) {
      this.destroy.emit(component);
    }
  }

  gridSizeChangedCallback(component) {
    if (this.gridSizeChanged) {
      this.gridSizeChanged.emit(component);
    }
  }

  itemChangeCallback(item, component) {
    if (this.itemChange) {
      this.itemChange.emit({item, component});
    }
  }

  itemResizeCallback(item, component) {
    if (this.itemResize) {
      this.itemResize.emit({item, component});
    }
  }

  itemInitCallback(item, component) {
    if (this.itemInit) {
      this.itemInit.emit({item, component});
    }
  }

  itemRemovedCallback(item, component) {
    if (this.itemRemoved) {
      this.itemRemoved.emit({item, component});
    }
  }

  itemValidateCallback(item) {
    if (this.itemValidate) {
      this.itemValidate.emit(item);
    }
  }

  onEdit($event: Dashart) {
    this.edit.emit($event);
  }

  onDelete($event: Dashart) {
    this.delete.emit($event);
  }

  ngOnInit() {
    this.gridsterConfig = {
      ...this.gridsterConfig, ...{
        initCallback: this.initCallback.bind(this),
        destroyCallback: this.destroyCallback.bind(this),
        gridSizeChangedCallback: this.gridSizeChangedCallback.bind(this),
        itemChangeCallback: this.itemChangeCallback.bind(this),
        itemResizeCallback: this.itemResizeCallback.bind(this),
        itemInitCallback: this.itemInitCallback.bind(this),
        itemRemovedCallback: this.itemRemovedCallback.bind(this),
        itemValidateCallback: this.itemValidateCallback.bind(this)
      }
    };

    this.menuItems = [
      {
        label: 'Update', icon: 'pi pi-refresh', command: () => {
        }
      },
      {
        label: 'Delete', icon: 'pi pi-times', command: () => {
        }
      },
      {label: 'Angular.io', icon: 'pi pi-info', url: 'http://angular.io'},
      {separator: true},
      {label: 'Setup', icon: 'pi pi-cog', routerLink: ['/setup']}
    ];
    console.log('DashartListComponent.ngOnInit()');

  }

}
