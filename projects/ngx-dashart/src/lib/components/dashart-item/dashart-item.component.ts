import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Dashart} from '../../model/vo/dashart';
import {Observable} from 'rxjs';
import {DashartData} from '../../model/vo/dashart-data';
import {createSelector, MemoizedSelector, select, Store} from '@ngrx/store';
import {map} from 'rxjs/operators';
import {DashartDataStoreSelectors} from '../../store/dashart-data-store';
import {getFlattenInObject} from '../../utils/j-utils';
import {MenuItem} from 'primeng/api';

export const selectAllFrom: (dashart: string) => MemoizedSelector<object, DashartData[]> = (dashart) => createSelector(
  DashartDataStoreSelectors.selectAll,
  (values) => (values.filter(value => value.dashart === dashart))
);

@Component({
  selector: 'app-dashart-item',
  templateUrl: './dashart-item.component.html',
  styles: [``]
})
export class DashartItemComponent implements OnInit {


  data$: Observable<any>;
  dashartB: Dashart;
  items: MenuItem[];

  constructor(private store$: Store<any>) {
  }

  @Input()
  set dashart(value: Dashart) {
    this.dashartB = JSON.parse(JSON.stringify(value));
  }

  @Output() edit = new EventEmitter<Dashart>();

  @Output() delete = new EventEmitter<Dashart>();

  ngOnInit() {
    this.items = [
      {
        label: 'Edit',
        icon: 'far fa-edit',
        command: () => {
          console.log('DashartItemComponent.command()');
          this.edit.emit(this.dashartB);
        }
      },
      {
        label: 'Delete',
        icon: 'far fa-trash-alt',
        command: () => {
          console.log('DashartItemComponent.command()');
          this.delete.emit(this.dashartB);
        }
      }
    ];

    this.data$ = this.store$.pipe(
      select(selectAllFrom(this.dashartB.id)),
      map(values => parseData(values))
    );
  }

}

export const parseData = (items: DashartData[]) => {
  const labels = getFlattenInObject('label', items, true);
  const datasetsTemp: { [key: string]: any[] } = {};
  items.forEach((value, index, array) => {
    if (!datasetsTemp.hasOwnProperty(value.group)) {
      datasetsTemp[value.group] = [];
    }
    datasetsTemp[value.group].push(value.value);
  });

  const keys = Object.keys(datasetsTemp);
  const datasets = [];
  keys.forEach(label => {
    datasets.push({label, data: datasetsTemp[label]});
  });

  return {labels, datasets};
};
